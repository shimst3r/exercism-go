# Exercism Go

## Summary
My solutions for the [Go](https://golang.org) track on the learning platform [exercism.io](https://exercism.io).

exercism.io is a platform for learning new languages from volunteer mentors. The exercises are set up as small test-driven development tasks and the solutions are discussed until they meet the language's idioms and best practises.

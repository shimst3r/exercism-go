//Package protein provides protein translation functions.
package protein

import "errors"

const (
	//codonLength is the length of a string representing a codon.
	codonLength int = 3
)

var (
	//ErrStop is returned when parsing a STOP codon.
	ErrStop = errors.New("STOP")
	//ErrInvalidBase is returned when parsing an invalid codon.
	ErrInvalidBase = errors.New("InvalidBase")
)

//FromCodon translates a codon to its protein.
func FromCodon(in string) (string, error) {
	switch in {
	case "AUG":
		return "Methionine", nil
	case "UUU", "UUC":
		return "Phenylalanine", nil
	case "UUA", "UUG":
		return "Leucine", nil
	case "UCU", "UCC", "UCA", "UCG":
		return "Serine", nil
	case "UAU", "UAC":
		return "Tyrosine", nil
	case "UGU", "UGC":
		return "Cysteine", nil
	case "UGG":
		return "Tryptophan", nil
	case "UAA", "UAG", "UGA":
		return "", ErrStop
	default:
		return "", ErrInvalidBase
	}
}

//FromRNA translates DNA to a list of proteins.
func FromRNA(in string) ([]string, error) {
	var result []string

	for idx := 0; idx < len(in); idx += codonLength {
		codon := in[idx : idx+codonLength]

		protein, err := FromCodon(codon)

		if err == ErrStop {
			return result, nil
		}
		if err == ErrInvalidBase {
			return result, ErrInvalidBase
		}
		result = append(result, protein)
	}
	return result, nil
}

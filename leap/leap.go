//Package leap provides a function to test for leap years.
package leap

//IsLeapYear tests if a given year is a leap year.
func IsLeapYear(year int) bool {
	//A year that's divisible by 4 is potentially a leap year
	if year%4 == 0 {
		//If the year is in addition divisible by 100 but NOT by 400,
		//it's not a leap year.
		if year%100 == 0 && year%400 != 0 {
			return false
		}
		return true
	}
	return false
}

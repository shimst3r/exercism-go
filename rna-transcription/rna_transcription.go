//Package strand provides functionality to map DNA to RNA.
package strand

import "strings"

//ToRNA maps a DNA strand to its RNA complement.
func ToRNA(dna string) string {
	toRNA := func(r rune) rune {
		switch r {
		case 'G':
			return 'C'
		case 'C':
			return 'G'
		case 'T':
			return 'A'
		case 'A':
			return 'U'
		default:
			return r
		}
	}

	return strings.Map(toRNA, dna)
}

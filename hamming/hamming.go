//Package hamming provides functionality to compute two gene strands' Hamming difference.
package hamming

import (
	"fmt"
)

//Distance computes the Hamming difference between two gene strands.
func Distance(a, b string) (int, error) {
	//`a` and `b` must have the same length
	if len(a) != len(b) {
		return 0, fmt.Errorf("strands have different length: %d vs %d", len(a), len(b))
	}

	count := 0

	//Convert `a` and `b` to rune slices.
	ar, br := []rune(a), []rune(b)
	//Iterate over `ar` ...
	for idx := range ar {
		//... and compare to values of `br`.
		if ar[idx] != br[idx] {
			//Increase `count` if they differ.
			count++
		}
	}

	return count, nil
}

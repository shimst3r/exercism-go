//Package accumulate provides functionality to apply functions to slices.
package accumulate

//Accumulate applies a given function to the input slice.
func Accumulate(in []string, f func(string) string) (out []string) {
	if len(in) == 0 {
		return
	}
	for idx := range in {
		out = append(out, f(in[idx]))
	}
	return
}

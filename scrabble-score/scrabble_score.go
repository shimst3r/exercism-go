//Package scrabble provides functionality to compute Scrabble scores of words.
package scrabble

import (
	"fmt"
	"strings"
)

//scrabbleScore looks up the Scrabble score of a given rune.
func scrabbleScore(letter rune) (int, error) {
	switch letter {
	case 'A', 'E', 'I', 'O', 'U', 'L', 'N', 'R', 'S', 'T':
		return 1, nil
	case 'D', 'G':
		return 2, nil
	case 'B', 'C', 'M', 'P':
		return 3, nil
	case 'F', 'H', 'V', 'W', 'Y':
		return 4, nil
	case 'K':
		return 5, nil
	case 'J', 'X':
		return 8, nil
	case 'Q', 'Z':
		return 10, nil
	default:
		return 0, fmt.Errorf("not a valid Scrabble letter: %v", letter)
	}
}

//Score computes the Scrabble score of a given word.
func Score(word string) (score int) {
	for _, letter := range strings.ToUpper(word) {
		value, err := scrabbleScore(letter)
		if err != nil {
			panic(err)
		}
		score += value
	}
	return
}

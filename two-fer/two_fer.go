//Package twofer provides a function to share things with other people.
package twofer

import "fmt"

//ShareWith takes an input string `name` and yields a share message.
func ShareWith(name string) string {
	if name == "" {
		name = "you"
	}
	return fmt.Sprintf("One for %s, one for me.", name)
}

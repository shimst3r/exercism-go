//Package strain implements functionality for discarding or keeping elements
//from a slice.
package strain

// *****
// Ints
// *****

//Ints is an array of integers.
type Ints []int

//Keep filters Ints using a predicate and keeps all true results.
func (i Ints) Keep(p func(int) bool) Ints {
	var result Ints

	for _, value := range i {
		if p(value) {
			result = append(result, value)
		}
	}
	return result

}

//Discard filters Ints using a predicate and discards all true results.
func (i Ints) Discard(p func(int) bool) Ints {
	var result Ints

	for _, value := range i {
		if !p(value) {
			result = append(result, value)
		}
	}
	return result
}

// *****
// Lists
// *****

//Lists is an array of arrays of integers.
type Lists [][]int

//Keep filters Lists using a predicate and keeps all true results.
func (l Lists) Keep(p func([]int) bool) Lists {
	var result Lists

	for _, value := range l {
		if p(value) {
			result = append(result, value)
		}
	}
	return result
}

// *****
// Strings
// *****

//Strings is an array of strings.
type Strings []string

//Keep filters Strings using a predicate and keeps all true results.
func (s Strings) Keep(p func(string) bool) Strings {
	var result Strings

	for _, value := range s {
		if p(value) {
			result = append(result, value)
		}
	}
	return result
}

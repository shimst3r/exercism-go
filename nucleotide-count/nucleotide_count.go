//Package dna provides functionality to count the number of nucleotides in a strand of DNA.
package dna

import "fmt"

//Nucleotide is a part of the DNA.
type Nucleotide rune

//Histogram is a mapping from nucleotide to its count in given DNA.
type Histogram map[Nucleotide]int

//DNA is a list of nucleotides.
type DNA []Nucleotide

// Counts generates a histogram of valid nucleotides in the given DNA.
// Returns an error if d contains an invalid nucleotide.
func (d DNA) Counts() (Histogram, error) {
	h := Histogram{'A': 0, 'C': 0, 'G': 0, 'T': 0}
	for _, nucleotide := range d {
		switch nucleotide {
		case 'A', 'C', 'G', 'T':
			h[nucleotide]++
		default:
			return nil, fmt.Errorf("%v contains illegal nucleotide %v", d, nucleotide)
		}
	}
	return h, nil
}

//Package raindrops provides functionality to convert a number to raindrops,
//based on its factors.
package raindrops

import "strconv"

//Convert converts a number to its raindrop representation. For factors 3, 5, and 7
//the respective raindrop sounds "Pling", "Plang", and "Plong" are returned. If neither
//is a factor of `number`, itself will be returned as a string.
func Convert(number int) string {
	var raindrops string
	if number%3 == 0 {
		raindrops += "Pling"
	}
	if number%5 == 0 {
		raindrops += "Plang"
	}
	if number%7 == 0 {
		raindrops += "Plong"
	}
	if len(raindrops) > 0 {
		return raindrops
	}
	return strconv.Itoa(number)
}

//Package isogram checks words to be isograms.
//
//Isograms are words or phrases without a repeating letter, excluding
//spaces and hyphens.
package isogram

import "strings"

//IsIsogram returns `true` if the input is an isogram, `false` otherwise.
func IsIsogram(in string) bool {
	//My implementation of `IsIsogram` uses a map to mark the presence of runes
	//in the input string.
	presenceMap := make(map[rune]bool)

	for _, r := range strings.ToLower(in) {
		if _, prs := presenceMap[r]; prs {
			if r != ' ' && r != '-' {
				return false
			}
		}
		presenceMap[r] = true
	}
	return true
}
